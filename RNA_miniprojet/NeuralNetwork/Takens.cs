﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RNA_miniprojet.Matrix;

namespace RNA_miniprojet.NeuralNetwork
{
    class Takens
    {
        public double[] Serie { get; set; }

        private double[] xBar;
        private int tau;
        private int i;
        private int n;

        private double[] eigenValues;

        private Matrix.Matrix covariance;

        public Takens(double[] serie, int n, int beginFrom, int tau)
        {
            Serie = serie;
            xBar = new double[n];
            double _tau = (serie.Length - beginFrom) / (n - 1);
            if (tau > _tau)
            {
                throw new Exception("Tau is incorrect");
            }
            this.tau = tau;

            if (beginFrom > (serie.Length - n - 1))
            {
                throw new Exception("Index to begin from out of bounds.");
            }
            int _n = serie.Length;
            //tau = (_n - 1 - beginFrom) / (n - 1);
            //tau = 1;
            Console.WriteLine($"Tau = {tau}");

            i = beginFrom;
            this.n = n;
        }

        private void createXBar()
        {
            for (int j = 0; j < n; j++)
            {
                xBar[j] = Serie[i + j * tau];
            }
        }

        private void createCovarianceMatrix()
        {
            covariance = new Matrix.Matrix(xBar.Length, xBar.Length);
            for (int j = 0; j < xBar.Length; j++)
            {
                for (int k = 0; k < xBar.Length; k++)
                {
                    covariance[j, k] = xBar[j] * xBar[k];
                }
            }
        }

        private double[] computeAverageError(double[] eigenValues)
        {
            List<double> tmp = eigenValues.ToList<double>();
            tmp.Sort(new DoubleDescendingComparer());
            eigenValues = tmp.ToArray();
            this.eigenValues = tmp.ToArray();

            double[] epsilon = new double[eigenValues.Length];

            for (int j = 0; j < eigenValues.Length; j++)
            {
                epsilon[j] = Math.Sqrt(eigenValues[j] + 1.0);
            }

            return epsilon;
        }

        public int GetInputUnitCount()
        {
            int inputUnitCount = 0;
            createXBar();
            createCovarianceMatrix();

            eigenValues = covariance.EigenValues();

            double[] epsilon = computeAverageError(eigenValues);

            for (int j = 0; j < epsilon.Length - 1; j++)
            {
                //if (Math.Abs(epsilon[j + 1] - epsilon[j]) < 0.00001)
                if (epsilon[j + 1] == epsilon[j])
                {
                    inputUnitCount = j + 1;
                    break;
                }
                else if (Math.Abs(epsilon[j + 1] - epsilon[j]) < 0.001)
                {
                    inputUnitCount = j + 1;
                    break;
                }
            }

            Console.WriteLine($"Input Unit Count : {inputUnitCount}");
            return inputUnitCount;
        }

        public double[] XBar
        {
            get
            {
                return xBar;
            }
        }

        public double[] EigenValues
        {
            get
            {
                return eigenValues;
            }
        }
    }
}
