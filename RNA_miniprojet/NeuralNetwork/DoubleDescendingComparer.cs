﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.NeuralNetwork
{
    class DoubleDescendingComparer : IComparer<double>
    {
        public int Compare(double x, double y)
        {
            if (x == y) return 0;
            else if (x < y) return 1;
            else return -1;
        }
    }
}
