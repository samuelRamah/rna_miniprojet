﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.NeuralNetwork
{
    class Point : IComparable<Point>
    {
        public double X { get; set; }
        public double Y { get; set; }

        public int CompareTo(Point other)
        {
            if (this.X == other.X) return 0;
            else if (this.X < other.X) return -1;
            else return 1;
        }

        public override string ToString()
        {
            return $"x = {X} -- y = {Y}";
        }
    }
}
