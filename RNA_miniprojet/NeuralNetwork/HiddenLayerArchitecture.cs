﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.NeuralNetwork
{
    class HiddenLayerArchitecture
    {
        private const int MAX_HIDDEN_UNIT_COUNT = 50;

        private int inputUnitCount;
        private double[] data;

        public HiddenLayerArchitecture(double[] data, int inputUnitCount)
        {
            this.inputUnitCount = inputUnitCount;
            this.data = data;
        }

        public int GetHiddenLayerUnitCount()
        {
            int hiddenUnitCount = 1;

            NeuralNetwork rna = new NeuralNetwork(inputUnitCount, 1, data);
            rna.Learn();

            double minNmse = rna.NMSE;

            for (int i = 2; i < inputUnitCount; i++)
            {
                rna = new NeuralNetwork(inputUnitCount, i, data);
                rna.Learn();
                double nmse = rna.NMSE;
                if( nmse < minNmse)
                {
                    minNmse = nmse;
                    hiddenUnitCount = i;
                }
            }

            return hiddenUnitCount;
        }
    }
}
