﻿using RNA_miniprojet.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.NeuralNetwork
{
    delegate double function(double x);

    class NeuralNetwork
    {
        private const int M = 3; // Layers count

        private const double NANO = 0.1; // learning step

        private const int PROTOTYPE_COUNT = 50;

        public double[] TrainingData { get; set; }

        // Weights
        // Dimension 0 : index of the layer
        // Dimension 1 : starting node
        // Dimension 2 : ending node
        private double[][][] w;

        // Input quantities
        // Dimension 0 : index of the layer ;
        // Dimension 1 : index of the node
        private double[][] h;

        // Output quantities
        // Dimension 0 : index of the layer ;
        // Dimension 1 : index of the node
        private double[][] v;

        // Delta
        private double[][] delta;

        // Prototype
        private double[] u;

        private function[] f;
        private function[] fPrime;

        private double[] obtained;

        public int InputUnitCount { get; set; }
        public int HiddenUnitCount { get; set; }

        private double nmse;
        public double NMSE
        {
            get
            {
                for (int i = 0; i < PROTOTYPE_COUNT; i++)
                {
                    double x = PredictOneStep(i + InputUnitCount + 1);
                    nmse += Math.Pow((TrainingData[InputUnitCount + i + 1] - x), 2);
                }
                
                double[] serie = new double[PROTOTYPE_COUNT + InputUnitCount - 1];
                Array.ConstrainedCopy(TrainingData, 0, serie, 0, PROTOTYPE_COUNT + InputUnitCount - 1);
                
                nmse /= PROTOTYPE_COUNT;
                nmse /= Stats.Variance(serie);

                return nmse;
            }
            set
            {
                nmse = value;
            }
        }

        private double signoide(double x)
        {
            return 1 / (1 + Math.Exp(-x));
        }

        private double signoidePrime(double x)
        {
            return Math.Exp(-x) / Math.Pow(1 + Math.Exp(-x), 2);
        }

        private void initLearningFunctions()
        {
            f = new function[M - 1];
            fPrime = new function[M - 1];

            for (int i = 0; i < M - 2; i++)
            {
                f[i] = signoide;
                fPrime[i] = signoidePrime;
            }

            f[M - 2] = (x) => x; // Identity
            fPrime[M - 2] = (x) => 1; // Identity prime
        }

        public NeuralNetwork(int inputUnitCount, int hiddenUnitCount, double[] trainingData)
        {
            initLearningFunctions();
            h = new double[M][];
            h[0] = new double[inputUnitCount];
            h[1] = new double[hiddenUnitCount];
            h[M - 1] = new double[1];

            v = new double[M][];
            v[0] = new double[inputUnitCount];
            v[1] = new double[hiddenUnitCount];
            v[M - 1] = new double[1];

            w = new double[M - 1][][];
            w[0] = new double[inputUnitCount][];
            for (int i = 0; i < inputUnitCount; i++)
            {
                w[0][i] = new double[hiddenUnitCount];
            }
            w[1] = new double[hiddenUnitCount][];
            for (int i = 0; i < hiddenUnitCount; i++)
            {
                w[1][i] = new double[1];
            }

            delta = new double[M - 1][];
            delta[0] = new double[hiddenUnitCount];
            delta[1] = new double[1];

            InputUnitCount = inputUnitCount;
            HiddenUnitCount = hiddenUnitCount;

            obtained = new double[trainingData.Length - inputUnitCount];

            TrainingData = trainingData;
        }

        public NeuralNetwork(int inputUnitCount, int hiddenUnitCount) : this(inputUnitCount, hiddenUnitCount, new double[1])
        {

        }

        private void initWeights()
        {
            Random rand = new Random();
            for (int i = 0; i < M - 1; i++)
            {
                int startingUnitCount = h[i].Length;
                for (int j = 0; j < startingUnitCount; j++)
                {
                    int destinationUnitCount = w[i][j].Length;
                    for (int k = 0; k < destinationUnitCount; k++)
                    {
                        w[i][j][k] = (rand.NextDouble() / 3.0) * ((rand.Next() % 10) < 5 ? 1 : -1);
                        //w[i][j][k] = 0.1;
                    }
                }
            }
        }

        private double[] choosePrototype(double[] data, int index = 0)
        {
            double[] result = new double[InputUnitCount];
            Array.Copy(data, index, result, 0, InputUnitCount);

            return result;
        }

        private void spreadForward(double[] prototype)
        {
            prototype.CopyTo(h[0], 0);
            prototype.CopyTo(v[0], 0);
            for (int m = 0; m < M - 1; m++)
            {
                int destinationNodeCount = h[m + 1].Length;
                for (int i = 0; i < destinationNodeCount; i++)
                {
                    h[m + 1][i] = 0;
                    int sourceNodeCount = h[m].Length;
                    for (int j = 0; j < sourceNodeCount; j++)
                    {
                        h[m + 1][i] += w[m][j][i] * v[m][j];
                    }
                    v[m + 1][i] = f[m](h[m + 1][i]);
                }
            }
        }

        private void computeDeltaOutput(double expected, double obtained)
        {
            double tmp = fPrime[M - 2](h[M - 1][0]);
            delta[1][0] = tmp * (expected - obtained);
        }

        private void spreadBackward()
        {
            for (int m = M - 2; m > 0; m--)
            {
                for (int i = 0; i < h[m].Length; i++)
                {
                    delta[m - 1][i] = fPrime[m - 1](h[m][i]);

                    double tmp = 0;
                    int destinationNodeCount = w[m][i].Length;
                    for (int j = 0; j < destinationNodeCount; j++)
                    {
                        tmp += w[m][i][j] * delta[m][j];
                    }

                    delta[m - 1][i] *= tmp;
                }
            }
        }

        private void updateWeights()
        {
            double dW = 0;
            for (int m = 0; m < M - 1; m++)
            {
                for (int i = 0; i < w[m].Length; i++)
                {
                    for (int j = 0; j < w[m][i].Length; j++)
                    {
                        dW = NANO * delta[m][j] * v[m][i];
                        w[m][i][j] += dW;
                    }
                }
            }
        }

        private void learn(int beginFrom = 0)
        {
            double predictedOutput = 0;
            double expectedOutput = 0;

            for (int i = 0; i < PROTOTYPE_COUNT; i++)
            {
                u = choosePrototype(TrainingData, beginFrom + i);
                spreadForward(u);

                predictedOutput = v[M - 1][0];
                expectedOutput = TrainingData[InputUnitCount + beginFrom + i];

                computeDeltaOutput(expectedOutput, predictedOutput);
                spreadBackward();
                updateWeights();
            }
        }

        public void Learn()
        {
            initWeights();
            learn();
            double last_nmse = NMSE;
            double new_nmse = 0;

            while(true)
            {
                learn();
                new_nmse = NMSE;
                if (new_nmse > last_nmse)
                {
                    // Surapprentissage
                    break;
                }
                last_nmse = new_nmse;
            }

        }

        private double predict(double[] u)
        {
            double x = 0;

            spreadForward(u);

            x = v[M - 1][0];

            return x;
        }

        public double PredictOneStep(int n)
        {
            double x = 0;

            x = predict(choosePrototype(TrainingData, n - InputUnitCount));

            return x;
        }

        public double[] PredictMultiStep(int n, int step)
        {
            double[] x = new double[step];

            double[] prototype = choosePrototype(TrainingData, n - InputUnitCount - 1);
            x[0] = predict(prototype);

            for (int i = 1; i < step; i++)
            {
                // Backward retropropagation
                Array.Copy(prototype, 0, prototype, 1, prototype.Length - 1);
                prototype[0] = x[i - 1];
                x[i] = predict(prototype);
            }
            
            return x;
        }

        public void SetFunction(int layer, function f, function fPrime)
        {
            if (layer > M)
            {
                throw new Exception("Out of bounds");
            }
            this.f[layer] = f;
            this.fPrime[layer] = fPrime;
        }

        public double[][][] Weights
        {
            get
            {
                return w;
            }
        }
    }
}
