﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.NeuralNetwork
{
    class Henon
    {
        public double A { get; set; }
        public double B { get; set; }
        public int N { get; set; }

        public double[] X { get; private set; }
        public double[] Y { get; private set; }
        public List<Point> Points { get; set; }

        public Henon(): this(500, 1.4, 0.3)
        {

        }

        public Henon(int n, double a, double b)
        {
            A = a;
            B = b;
            N = n;
            X = new double[n];
            Y = new double[n];
            Points = new List<Point>();
        }

        public Henon Generate()
        {
            double x0 = 0;
            double y0 = 0;

            double x1 = x0;
            double y1 = y0;

            for (int i = 0; i < N; i++)
            {
                X[i] = x1;
                Y[i] = y1;
                Points.Add(new Point()
                {
                    X = x1,
                    Y = y1
                });

                x0 = x1;
                y0 = y1;

                x1 = y0 + 1 - A * x0 * x0;
                y1 = B * x0;
            }

            return this;
        }

    }
}
