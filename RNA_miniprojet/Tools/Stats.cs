﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.Tools
{
    static class Stats
    {
        public static double Average(double[] data)
        {
            double average = 0;
            for (int i = 0; i < data.Length; i++)
            {
                average += data[i];
            }
            average /= data.Length;
            return average;
        }

        public static double Variance(double[] data)
        {
            double variance = 0;
            double average = Stats.Average(data);

            for (int i = 0; i < data.Length; i++)
            {
                variance += Math.Pow(data[i], 2);
            }
            variance /= data.Length;
            variance -= Math.Pow(average, 2);

            return variance;
        }

        public static double StdDeviation(double[] data)
        {
            return Math.Sqrt(Stats.Variance(data));
        }

    }
}

