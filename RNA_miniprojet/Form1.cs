﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RNA_miniprojet.Matrix;
using RNA_miniprojet.NeuralNetwork;

namespace RNA_miniprojet
{
    public partial class Form1 : Form
    {
        private Henon henon;
        private Takens takens;
        private int inputUnitCount = 0;
        private int hiddenUnitCount = 0;

        private double a = 0;
        private double b = 0;

        NeuralNetwork.NeuralNetwork rna;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            henonTab(500, 1.4, 0.3);
            takensTab();
        }

        private void testMatrixEigenValues()
        {
            //double[,] data = new double[,]
            //{
            //    {9,    0,    -1,    0.242,      6,    1},
            //    {3,   -2,     8,        1,      3,    2},
            //    {2,    0,    12,    -3.48,   2.54,    1},
            //    {0,    3,  4.68,       -2,     -1,    4},
            //    {5,    1,     1,        0,      0,    5},
            //    {2,    7,     2,        3,      4,    1},
            //};

            //double [,] data = new double[,]
            //{ { 1.0000,    0.5000,    0.3333,    0.2500 },
            // { 0.5000,    1.0000,    0.6667,    0.5000 },
            // { 0.3333,    0.6667,    1.0000,    0.7500 },
            // { 0.2500,    0.5000,    0.7500,    1.0000 }};

            double[,] data = new double[,] { { 2.3, 4.5 }, { 6.7, -1.2 } };
            //double[,] data = new double[,] { { 2, 1 }, { 1, 2 } };
            //double[,] data = new double[,] { { 4, 1 }, { 6, 3 } };
            //double[,] data = new double[,] { { 2, 5, -2 }, { 1, 4, -2 }, { 0, -3, 5 } };
            //double[,] data = new double[,] { { 3, 2, 6 }, { 2, 2, 5 }, { -2, -1, -4} };
            //double[,] data = new double[,] { { 2, 0, 0 }, { 0, 3, 4 }, { 0, 4, 9 } };

            Matrix.Matrix m = new Matrix.Matrix(data);

            foreach (var item in m.EigenValues())
            {
                Console.WriteLine(item);
            }
        }

        private void henonTab(int n, double a, double b)
        {
            this.a = a;
            this.b = b;

            dgvHenon.ColumnCount = 3;
            dgvHenon.Columns[0].Name = "i";
            dgvHenon.Columns[1].Name = "Xn";
            dgvHenon.Columns[2].Name = "Yn";

            dgvHenon.Rows.Clear();

            henon = new Henon(n, a, b).Generate();
            int i = 0;
            foreach (var item in henon.Points)
            {
                dgvHenon.Rows.Add(new string[]
                {
                    ""+i,
                    ""+Math.Round(item.X, 8),
                    ""+Math.Round(item.Y, 8)
                });
                i++;
            }
            
            henon.Points.Sort();
            chartHenon.Series["Henon"].Points.Clear();
            chartHenon.ResetAutoValues();
            foreach (var item in henon.Points)
            {
                chartHenon.Series["Henon"].Points.AddXY(Math.Round(item.X, 8), Math.Round(item.Y, 8));
            }
        }

        private void btnHenonCompute_Click(object sender, EventArgs e)
        {
            double a = 0;
            double b = 0;
            int n = 0;

            if (txtA.Text != "")
            {
                try
                {
                    a = double.Parse(txtA.Text.Replace('.', ','));
                }
                catch (Exception)
                {
                    Console.WriteLine("Tsy mety ny A");
                }
            }

            if (txtB.Text != "")
            {
                try
                {
                    b = double.Parse(txtB.Text.Replace('.', ','));
                }
                catch (Exception)
                {
                    Console.WriteLine("Tsy mety ny B");
                }
            }

            if (txtN.Text != "")
            {
                try
                {
                    n = int.Parse(txtN.Text);
                }
                catch (Exception)
                {
                    Console.WriteLine("Tsy mety ny N");
                }
            }

            henonTab(n, a, b);
        }

        private void takensTab()
        {
            dgvXBar.ColumnCount = 2;
            dgvXBar.Columns[0].Name = "i";
            dgvXBar.Columns[1].Name = "x[i]";
            dgvXBar.Rows.Clear();

            dgvEigenValues.ColumnCount = 2;
            dgvEigenValues.Columns[0].Name = "l";
            dgvEigenValues.Columns[1].Name = "Lambda";
            dgvEigenValues.Rows.Clear();

            lblTakensCount.Text = ""+ inputUnitCount;

            chartTakens.Series["Takens"].Points.Clear();
            chartTakens.Series["Premier plateau"].Points.Clear();
            chartTakens.ResetAutoValues();
        }

        private void btnArchitecture_Click(object sender, EventArgs e)
        {
            takensTab();
            btnArchitectureCompute.Text = "Veuillez patienter ...";
            btnArchitectureCompute.Enabled = false;

            int i = getInt(txtTakensI.Text);
            int n = getInt(txtTakensN.Text);
            int tau = getInt(txtTakensTau.Text);

            takens = new Takens(henon.X, n, i, tau);

            inputUnitCount = takens.GetInputUnitCount();

            for (int j = 0; j < takens.XBar.Length; j++)
            {
                dgvXBar.Rows.Add(new string[] {
                    ""+j,
                    ""+Math.Round(takens.XBar[j], 8)
                });
            }

            for (int j = 0; j < takens.EigenValues.Length; j++)
            {
                dgvEigenValues.Rows.Add(new string[]
                {
                    ""+(j+1),
                    ""+Math.Round(takens.EigenValues[j], 8)
                });
                if (j != 0 && j < 30) chartTakens.Series["Takens"].Points.AddXY(j + 1, takens.EigenValues[j]);
            }

            chartTakens.Series["Premier plateau"].Points.AddXY(inputUnitCount, takens.EigenValues[inputUnitCount - 1]);
            lblTakensCount.Text = ""+inputUnitCount;

            hiddenUnitCount = new HiddenLayerArchitecture(henon.X, inputUnitCount).GetHiddenLayerUnitCount();
            lblTakensHidden.Text = ""+hiddenUnitCount;

            btnArchitectureCompute.Text = "Calculer";
            btnArchitectureCompute.Enabled = true;

        }

        private int getInt(string s)
        {
            if (s != "")
            {
                try
                {
                    return int.Parse(s);
                }
                catch
                {

                }
            }
            return 0;
        }

        private void btnLearn_Click(object sender, EventArgs e)
        {
            btnLearn.Enabled = false;
            btnLearn.Text = "Veuillez patienter";

            dgvLearnHidden.ColumnCount = dgvLearnInput.ColumnCount = 2;
            dgvLearnInput.Columns[0].Name = dgvLearnHidden.Columns[0].Name = "";
            dgvLearnInput.Columns[1].Name = dgvLearnHidden.Columns[1].Name = "Wji";

            dgvLearnHidden.Rows.Clear();
            dgvLearnInput.Rows.Clear();

            rna = new NeuralNetwork.NeuralNetwork(inputUnitCount, hiddenUnitCount, henon.X);
            rna.Learn();
            double[][][] w = rna.Weights;

            for (int i = 0; i < inputUnitCount; i++)
            {
                for (int j = 0; j < hiddenUnitCount; j++)
                {
                    dgvLearnInput.Rows.Add(new string[]
                    {
                        $"W[{i}][{j}]",
                        ""+ w[0][i][j],
                    });
                }
            }

            for (int i = 0; i < hiddenUnitCount; i++)
            {
                dgvLearnHidden.Rows.Add(new string[]
                {
                        $"W[{i}][0]",
                        ""+ w[1][i][0],
                });
            }

            btnLearn.Enabled = true;
            btnLearn.Text = "Relancer l'apprentissage (nouveau poids initiaux aléatoire)";
        }

        private void btnPredictionOne_Click(object sender, EventArgs e)
        {
            int beginFrom = 200;
            int toPredict = 10;
            double[] data = new double[toPredict];
            double[] obtained = new double[toPredict];
            double y = 0;

            chartPredictionOne.Series[0].Points.Clear();
            chartPredictionOne.Series[1].Points.Clear();


            dgvPredictionOne.ColumnCount = 3;
            dgvPredictionOne.Columns[0].Name = "Attendue";
            dgvPredictionOne.Columns[1].Name = "Prédite";
            dgvPredictionOne.Columns[2].Name = "Delta erreur";

            dgvPredictionOne.Rows.Clear();

            y = henon.Y[beginFrom];

            Array.Copy(henon.X, beginFrom, data, 0, toPredict);
            for (int i = 0; i < toPredict; i++)
            {
                obtained[i] = rna.PredictOneStep(beginFrom + i);

                chartPredictionOne.Series[0].Points.AddXY(henon.X[beginFrom + i], henon.Y[beginFrom + i]);
                //chartPredictionOne.Series[1].Points.AddXY(obtained[i], henon.Y[beginFrom + i]);
                chartPredictionOne.Series[1].Points.AddXY(obtained[i], y);


                dgvPredictionOne.Rows.Add(new string[]
                {
                    ""+Math.Round(henon.X[beginFrom + i], 8),
                    ""+Math.Round(obtained[i], 8),
                    ""+Math.Round(-henon.X[beginFrom + i] + obtained[i], 8)
                });

                y = b * obtained[i];
            }
        }

        private void btnPredictionMulti_Click(object sender, EventArgs e)
        {
            int beginFrom = 200;
            int toPredict = int.Parse(cmbPredictionMultiPas.Text);
            double[] data = new double[toPredict];
            double[] obtained = new double[toPredict];
            double y;

            chartPredictionMulti.Series[0].Points.Clear();
            chartPredictionMulti.Series[1].Points.Clear();

            dgvPredictionMulti.ColumnCount = 3;
            dgvPredictionMulti.Columns[0].Name = "Attendue";
            dgvPredictionMulti.Columns[1].Name = "Prédite";
            dgvPredictionMulti.Columns[2].Name = "Delta erreur";

            dgvPredictionMulti.Rows.Clear();

            obtained = rna.PredictMultiStep(beginFrom, toPredict);
            y = henon.Y[beginFrom];

            for (int i = 0; i < toPredict; i++)
            {
                chartPredictionMulti.Series[0].Points.AddXY(henon.X[beginFrom + i], henon.Y[beginFrom + i]);
                //chartPredictionOne.Series[1].Points.AddXY(obtained[i], henon.Y[beginFrom + i]);
                chartPredictionMulti.Series[1].Points.AddXY(obtained[i], y);


                dgvPredictionMulti.Rows.Add(new string[]
                {
                    ""+Math.Round(henon.X[beginFrom + i], 8),
                    ""+Math.Round(obtained[i], 8),
                    ""+Math.Round(-henon.X[beginFrom + i] + obtained[i], 8)
                });

                y = b * obtained[i];
            }


        }
    }
}
