﻿namespace RNA_miniprojet
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtN = new System.Windows.Forms.TextBox();
            this.lblN = new System.Windows.Forms.Label();
            this.txtB = new System.Windows.Forms.TextBox();
            this.lblB = new System.Windows.Forms.Label();
            this.txtA = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnHenonCompute = new System.Windows.Forms.Button();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.dgvHenon = new System.Windows.Forms.DataGridView();
            this.chartHenon = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.splitContainer8 = new System.Windows.Forms.SplitContainer();
            this.txtTakensTau = new System.Windows.Forms.TextBox();
            this.lblTakensTau = new System.Windows.Forms.Label();
            this.txtTakensN = new System.Windows.Forms.TextBox();
            this.lblTakensN = new System.Windows.Forms.Label();
            this.txtTakensI = new System.Windows.Forms.TextBox();
            this.lblTakensI = new System.Windows.Forms.Label();
            this.splitContainer9 = new System.Windows.Forms.SplitContainer();
            this.btnArchitectureCompute = new System.Windows.Forms.Button();
            this.lblTakensHidden = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTakensCount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.splitContainer10 = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvXBar = new System.Windows.Forms.DataGridView();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.splitContainer11 = new System.Windows.Forms.SplitContainer();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvEigenValues = new System.Windows.Forms.DataGridView();
            this.chartTakens = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer12 = new System.Windows.Forms.SplitContainer();
            this.btnLearn = new System.Windows.Forms.Button();
            this.splitContainer13 = new System.Windows.Forms.SplitContainer();
            this.splitContainer14 = new System.Windows.Forms.SplitContainer();
            this.label8 = new System.Windows.Forms.Label();
            this.dgvLearnInput = new System.Windows.Forms.DataGridView();
            this.splitContainer15 = new System.Windows.Forms.SplitContainer();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvLearnHidden = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.splitContainer16 = new System.Windows.Forms.SplitContainer();
            this.splitContainer18 = new System.Windows.Forms.SplitContainer();
            this.txtPredictionOneCount = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPredictionOneN0 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnPredictionOne = new System.Windows.Forms.Button();
            this.chartPredictionOne = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.splitContainer17 = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer19 = new System.Windows.Forms.SplitContainer();
            this.dgvPredictionOne = new System.Windows.Forms.DataGridView();
            this.splitContainer20 = new System.Windows.Forms.SplitContainer();
            this.chartPredictionMulti = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.splitContainer21 = new System.Windows.Forms.SplitContainer();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbPredictionMultiPas = new System.Windows.Forms.ComboBox();
            this.dgvPredictionMulti = new System.Windows.Forms.DataGridView();
            this.btnPredictionMulti = new System.Windows.Forms.Button();
            this.splitContainer22 = new System.Windows.Forms.SplitContainer();
            this.splitContainer23 = new System.Windows.Forms.SplitContainer();
            this.splitContainer24 = new System.Windows.Forms.SplitContainer();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHenon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartHenon)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).BeginInit();
            this.splitContainer8.Panel1.SuspendLayout();
            this.splitContainer8.Panel2.SuspendLayout();
            this.splitContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).BeginInit();
            this.splitContainer9.Panel1.SuspendLayout();
            this.splitContainer9.Panel2.SuspendLayout();
            this.splitContainer9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).BeginInit();
            this.splitContainer10.Panel1.SuspendLayout();
            this.splitContainer10.Panel2.SuspendLayout();
            this.splitContainer10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvXBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).BeginInit();
            this.splitContainer11.Panel1.SuspendLayout();
            this.splitContainer11.Panel2.SuspendLayout();
            this.splitContainer11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEigenValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTakens)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).BeginInit();
            this.splitContainer12.Panel1.SuspendLayout();
            this.splitContainer12.Panel2.SuspendLayout();
            this.splitContainer12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).BeginInit();
            this.splitContainer13.Panel1.SuspendLayout();
            this.splitContainer13.Panel2.SuspendLayout();
            this.splitContainer13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).BeginInit();
            this.splitContainer14.Panel1.SuspendLayout();
            this.splitContainer14.Panel2.SuspendLayout();
            this.splitContainer14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLearnInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).BeginInit();
            this.splitContainer15.Panel1.SuspendLayout();
            this.splitContainer15.Panel2.SuspendLayout();
            this.splitContainer15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLearnHidden)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).BeginInit();
            this.splitContainer16.Panel1.SuspendLayout();
            this.splitContainer16.Panel2.SuspendLayout();
            this.splitContainer16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer18)).BeginInit();
            this.splitContainer18.Panel1.SuspendLayout();
            this.splitContainer18.Panel2.SuspendLayout();
            this.splitContainer18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPredictionOne)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer17)).BeginInit();
            this.splitContainer17.Panel1.SuspendLayout();
            this.splitContainer17.Panel2.SuspendLayout();
            this.splitContainer17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer19)).BeginInit();
            this.splitContainer19.Panel1.SuspendLayout();
            this.splitContainer19.Panel2.SuspendLayout();
            this.splitContainer19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPredictionOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer20)).BeginInit();
            this.splitContainer20.Panel1.SuspendLayout();
            this.splitContainer20.Panel2.SuspendLayout();
            this.splitContainer20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPredictionMulti)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer21)).BeginInit();
            this.splitContainer21.Panel1.SuspendLayout();
            this.splitContainer21.Panel2.SuspendLayout();
            this.splitContainer21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPredictionMulti)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer22)).BeginInit();
            this.splitContainer22.Panel1.SuspendLayout();
            this.splitContainer22.Panel2.SuspendLayout();
            this.splitContainer22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer23)).BeginInit();
            this.splitContainer23.Panel1.SuspendLayout();
            this.splitContainer23.Panel2.SuspendLayout();
            this.splitContainer23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer24)).BeginInit();
            this.splitContainer24.Panel1.SuspendLayout();
            this.splitContainer24.Panel2.SuspendLayout();
            this.splitContainer24.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1221, 732);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1213, 720);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Série Henon";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1207, 714);
            this.splitContainer2.SplitterDistance = 59;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.label4);
            this.splitContainer4.Panel1.Controls.Add(this.label3);
            this.splitContainer4.Panel1.Controls.Add(this.txtN);
            this.splitContainer4.Panel1.Controls.Add(this.lblN);
            this.splitContainer4.Panel1.Controls.Add(this.txtB);
            this.splitContainer4.Panel1.Controls.Add(this.lblB);
            this.splitContainer4.Panel1.Controls.Add(this.txtA);
            this.splitContainer4.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.btnHenonCompute);
            this.splitContainer4.Size = new System.Drawing.Size(1207, 59);
            this.splitContainer4.SplitterDistance = 1013;
            this.splitContainer4.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(425, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "y(n+1) = b * x(n)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(425, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "x(n+1) = y(n) + 1 - a * x(n) * x(n)";
            // 
            // txtN
            // 
            this.txtN.Location = new System.Drawing.Point(226, 4);
            this.txtN.Name = "txtN";
            this.txtN.Size = new System.Drawing.Size(100, 20);
            this.txtN.TabIndex = 5;
            this.txtN.Text = "500";
            // 
            // lblN
            // 
            this.lblN.AutoSize = true;
            this.lblN.Location = new System.Drawing.Point(197, 4);
            this.lblN.Name = "lblN";
            this.lblN.Size = new System.Drawing.Size(24, 13);
            this.lblN.TabIndex = 4;
            this.lblN.Text = "N : ";
            // 
            // txtB
            // 
            this.txtB.Location = new System.Drawing.Point(35, 30);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(100, 20);
            this.txtB.TabIndex = 3;
            this.txtB.Text = "0.3";
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Location = new System.Drawing.Point(6, 30);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(22, 13);
            this.lblB.TabIndex = 2;
            this.lblB.Text = "b : ";
            // 
            // txtA
            // 
            this.txtA.Location = new System.Drawing.Point(35, 4);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(100, 20);
            this.txtA.TabIndex = 1;
            this.txtA.Text = "1.4";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "a : ";
            // 
            // btnHenonCompute
            // 
            this.btnHenonCompute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHenonCompute.Location = new System.Drawing.Point(0, 0);
            this.btnHenonCompute.Name = "btnHenonCompute";
            this.btnHenonCompute.Size = new System.Drawing.Size(190, 59);
            this.btnHenonCompute.TabIndex = 0;
            this.btnHenonCompute.Text = "Générer";
            this.btnHenonCompute.UseVisualStyleBackColor = true;
            this.btnHenonCompute.Click += new System.EventHandler(this.btnHenonCompute_Click);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.dgvHenon);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.chartHenon);
            this.splitContainer3.Size = new System.Drawing.Size(1207, 651);
            this.splitContainer3.SplitterDistance = 365;
            this.splitContainer3.TabIndex = 0;
            // 
            // dgvHenon
            // 
            this.dgvHenon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHenon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHenon.Location = new System.Drawing.Point(0, 0);
            this.dgvHenon.Name = "dgvHenon";
            this.dgvHenon.Size = new System.Drawing.Size(365, 651);
            this.dgvHenon.TabIndex = 0;
            // 
            // chartHenon
            // 
            chartArea5.AxisX.Title = "Xn";
            chartArea5.AxisY.Title = "Yn";
            chartArea5.Name = "ChartArea1";
            this.chartHenon.ChartAreas.Add(chartArea5);
            this.chartHenon.Dock = System.Windows.Forms.DockStyle.Fill;
            legend5.Name = "Legend1";
            this.chartHenon.Legends.Add(legend5);
            this.chartHenon.Location = new System.Drawing.Point(0, 0);
            this.chartHenon.Name = "chartHenon";
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Legend = "Legend1";
            series8.Name = "Henon";
            this.chartHenon.Series.Add(series8);
            this.chartHenon.Size = new System.Drawing.Size(838, 651);
            this.chartHenon.TabIndex = 0;
            this.chartHenon.Text = "chart1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1213, 720);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Architecture du réseau";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(3, 3);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.splitContainer8);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer5.Size = new System.Drawing.Size(1207, 714);
            this.splitContainer5.SplitterDistance = 58;
            this.splitContainer5.TabIndex = 0;
            // 
            // splitContainer8
            // 
            this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer8.Location = new System.Drawing.Point(0, 0);
            this.splitContainer8.Name = "splitContainer8";
            // 
            // splitContainer8.Panel1
            // 
            this.splitContainer8.Panel1.Controls.Add(this.txtTakensTau);
            this.splitContainer8.Panel1.Controls.Add(this.lblTakensTau);
            this.splitContainer8.Panel1.Controls.Add(this.txtTakensN);
            this.splitContainer8.Panel1.Controls.Add(this.lblTakensN);
            this.splitContainer8.Panel1.Controls.Add(this.txtTakensI);
            this.splitContainer8.Panel1.Controls.Add(this.lblTakensI);
            // 
            // splitContainer8.Panel2
            // 
            this.splitContainer8.Panel2.Controls.Add(this.splitContainer9);
            this.splitContainer8.Size = new System.Drawing.Size(1207, 58);
            this.splitContainer8.SplitterDistance = 621;
            this.splitContainer8.TabIndex = 0;
            // 
            // txtTakensTau
            // 
            this.txtTakensTau.Location = new System.Drawing.Point(225, 2);
            this.txtTakensTau.Name = "txtTakensTau";
            this.txtTakensTau.Size = new System.Drawing.Size(100, 20);
            this.txtTakensTau.TabIndex = 11;
            this.txtTakensTau.Text = "5";
            // 
            // lblTakensTau
            // 
            this.lblTakensTau.AutoSize = true;
            this.lblTakensTau.Location = new System.Drawing.Point(191, 6);
            this.lblTakensTau.Name = "lblTakensTau";
            this.lblTakensTau.Size = new System.Drawing.Size(28, 13);
            this.lblTakensTau.TabIndex = 10;
            this.lblTakensTau.Text = "tau :";
            // 
            // txtTakensN
            // 
            this.txtTakensN.Location = new System.Drawing.Point(25, 29);
            this.txtTakensN.Name = "txtTakensN";
            this.txtTakensN.Size = new System.Drawing.Size(100, 20);
            this.txtTakensN.TabIndex = 9;
            this.txtTakensN.Text = "100";
            // 
            // lblTakensN
            // 
            this.lblTakensN.AutoSize = true;
            this.lblTakensN.Location = new System.Drawing.Point(4, 32);
            this.lblTakensN.Name = "lblTakensN";
            this.lblTakensN.Size = new System.Drawing.Size(19, 13);
            this.lblTakensN.TabIndex = 8;
            this.lblTakensN.Text = "n :";
            // 
            // txtTakensI
            // 
            this.txtTakensI.Location = new System.Drawing.Point(25, 3);
            this.txtTakensI.Name = "txtTakensI";
            this.txtTakensI.Size = new System.Drawing.Size(100, 20);
            this.txtTakensI.TabIndex = 7;
            this.txtTakensI.Text = "0";
            // 
            // lblTakensI
            // 
            this.lblTakensI.AutoSize = true;
            this.lblTakensI.Location = new System.Drawing.Point(4, 6);
            this.lblTakensI.Name = "lblTakensI";
            this.lblTakensI.Size = new System.Drawing.Size(15, 13);
            this.lblTakensI.TabIndex = 6;
            this.lblTakensI.Text = "i :";
            // 
            // splitContainer9
            // 
            this.splitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer9.Location = new System.Drawing.Point(0, 0);
            this.splitContainer9.Name = "splitContainer9";
            // 
            // splitContainer9.Panel1
            // 
            this.splitContainer9.Panel1.Controls.Add(this.btnArchitectureCompute);
            // 
            // splitContainer9.Panel2
            // 
            this.splitContainer9.Panel2.Controls.Add(this.lblTakensHidden);
            this.splitContainer9.Panel2.Controls.Add(this.label9);
            this.splitContainer9.Panel2.Controls.Add(this.lblTakensCount);
            this.splitContainer9.Panel2.Controls.Add(this.label5);
            this.splitContainer9.Size = new System.Drawing.Size(582, 58);
            this.splitContainer9.SplitterDistance = 274;
            this.splitContainer9.TabIndex = 0;
            // 
            // btnArchitectureCompute
            // 
            this.btnArchitectureCompute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnArchitectureCompute.Location = new System.Drawing.Point(0, 0);
            this.btnArchitectureCompute.Name = "btnArchitectureCompute";
            this.btnArchitectureCompute.Size = new System.Drawing.Size(274, 58);
            this.btnArchitectureCompute.TabIndex = 0;
            this.btnArchitectureCompute.Text = "Calculer";
            this.btnArchitectureCompute.UseVisualStyleBackColor = true;
            this.btnArchitectureCompute.Click += new System.EventHandler(this.btnArchitecture_Click);
            // 
            // lblTakensHidden
            // 
            this.lblTakensHidden.AutoSize = true;
            this.lblTakensHidden.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTakensHidden.Location = new System.Drawing.Point(246, 29);
            this.lblTakensHidden.Name = "lblTakensHidden";
            this.lblTakensHidden.Size = new System.Drawing.Size(0, 24);
            this.lblTakensHidden.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(236, 24);
            this.label9.TabIndex = 2;
            this.label9.Text = "Nombre d\'unités cachées :";
            // 
            // lblTakensCount
            // 
            this.lblTakensCount.AutoSize = true;
            this.lblTakensCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTakensCount.Location = new System.Drawing.Point(246, 6);
            this.lblTakensCount.Name = "lblTakensCount";
            this.lblTakensCount.Size = new System.Drawing.Size(0, 24);
            this.lblTakensCount.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(233, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "Nombre d\'unités d\'entrée :";
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.splitContainer10);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.splitContainer7);
            this.splitContainer6.Size = new System.Drawing.Size(1207, 652);
            this.splitContainer6.SplitterDistance = 194;
            this.splitContainer6.TabIndex = 0;
            // 
            // splitContainer10
            // 
            this.splitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer10.Location = new System.Drawing.Point(0, 0);
            this.splitContainer10.Name = "splitContainer10";
            this.splitContainer10.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer10.Panel1
            // 
            this.splitContainer10.Panel1.Controls.Add(this.label6);
            // 
            // splitContainer10.Panel2
            // 
            this.splitContainer10.Panel2.Controls.Add(this.dgvXBar);
            this.splitContainer10.Size = new System.Drawing.Size(194, 652);
            this.splitContainer10.SplitterDistance = 25;
            this.splitContainer10.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(71, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "XBar";
            // 
            // dgvXBar
            // 
            this.dgvXBar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvXBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvXBar.Location = new System.Drawing.Point(0, 0);
            this.dgvXBar.Name = "dgvXBar";
            this.dgvXBar.Size = new System.Drawing.Size(194, 623);
            this.dgvXBar.TabIndex = 0;
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.splitContainer11);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.chartTakens);
            this.splitContainer7.Size = new System.Drawing.Size(1009, 652);
            this.splitContainer7.SplitterDistance = 207;
            this.splitContainer7.TabIndex = 0;
            // 
            // splitContainer11
            // 
            this.splitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer11.Location = new System.Drawing.Point(0, 0);
            this.splitContainer11.Name = "splitContainer11";
            this.splitContainer11.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer11.Panel1
            // 
            this.splitContainer11.Panel1.Controls.Add(this.label7);
            // 
            // splitContainer11.Panel2
            // 
            this.splitContainer11.Panel2.Controls.Add(this.dgvEigenValues);
            this.splitContainer11.Size = new System.Drawing.Size(207, 652);
            this.splitContainer11.SplitterDistance = 25;
            this.splitContainer11.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Valeurs propres";
            // 
            // dgvEigenValues
            // 
            this.dgvEigenValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEigenValues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEigenValues.Location = new System.Drawing.Point(0, 0);
            this.dgvEigenValues.Name = "dgvEigenValues";
            this.dgvEigenValues.Size = new System.Drawing.Size(207, 623);
            this.dgvEigenValues.TabIndex = 0;
            // 
            // chartTakens
            // 
            chartArea6.Name = "ChartArea1";
            this.chartTakens.ChartAreas.Add(chartArea6);
            this.chartTakens.Dock = System.Windows.Forms.DockStyle.Fill;
            legend6.Name = "Legend1";
            this.chartTakens.Legends.Add(legend6);
            this.chartTakens.Location = new System.Drawing.Point(0, 0);
            this.chartTakens.Name = "chartTakens";
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Legend = "Legend1";
            series9.Name = "Takens";
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series10.Legend = "Legend1";
            series10.Name = "Premier plateau";
            this.chartTakens.Series.Add(series9);
            this.chartTakens.Series.Add(series10);
            this.chartTakens.Size = new System.Drawing.Size(798, 652);
            this.chartTakens.TabIndex = 0;
            this.chartTakens.Text = "chart1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer12);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1213, 720);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Apprentissage";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer12
            // 
            this.splitContainer12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer12.Location = new System.Drawing.Point(0, 0);
            this.splitContainer12.Name = "splitContainer12";
            this.splitContainer12.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer12.Panel1
            // 
            this.splitContainer12.Panel1.Controls.Add(this.btnLearn);
            // 
            // splitContainer12.Panel2
            // 
            this.splitContainer12.Panel2.Controls.Add(this.splitContainer13);
            this.splitContainer12.Size = new System.Drawing.Size(1213, 720);
            this.splitContainer12.SplitterDistance = 38;
            this.splitContainer12.TabIndex = 0;
            // 
            // btnLearn
            // 
            this.btnLearn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLearn.Location = new System.Drawing.Point(0, 0);
            this.btnLearn.Name = "btnLearn";
            this.btnLearn.Size = new System.Drawing.Size(1213, 38);
            this.btnLearn.TabIndex = 0;
            this.btnLearn.Text = "Lancer apprentissage";
            this.btnLearn.UseVisualStyleBackColor = true;
            this.btnLearn.Click += new System.EventHandler(this.btnLearn_Click);
            // 
            // splitContainer13
            // 
            this.splitContainer13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer13.Location = new System.Drawing.Point(0, 0);
            this.splitContainer13.Name = "splitContainer13";
            // 
            // splitContainer13.Panel1
            // 
            this.splitContainer13.Panel1.Controls.Add(this.splitContainer14);
            // 
            // splitContainer13.Panel2
            // 
            this.splitContainer13.Panel2.Controls.Add(this.splitContainer15);
            this.splitContainer13.Size = new System.Drawing.Size(1213, 678);
            this.splitContainer13.SplitterDistance = 605;
            this.splitContainer13.TabIndex = 0;
            // 
            // splitContainer14
            // 
            this.splitContainer14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer14.Location = new System.Drawing.Point(0, 0);
            this.splitContainer14.Name = "splitContainer14";
            this.splitContainer14.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer14.Panel1
            // 
            this.splitContainer14.Panel1.Controls.Add(this.label8);
            // 
            // splitContainer14.Panel2
            // 
            this.splitContainer14.Panel2.Controls.Add(this.dgvLearnInput);
            this.splitContainer14.Size = new System.Drawing.Size(605, 678);
            this.splitContainer14.SplitterDistance = 25;
            this.splitContainer14.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(247, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(189, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Poids couche entrée - couche cachée";
            // 
            // dgvLearnInput
            // 
            this.dgvLearnInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLearnInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLearnInput.Location = new System.Drawing.Point(0, 0);
            this.dgvLearnInput.Name = "dgvLearnInput";
            this.dgvLearnInput.Size = new System.Drawing.Size(605, 649);
            this.dgvLearnInput.TabIndex = 0;
            // 
            // splitContainer15
            // 
            this.splitContainer15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer15.Location = new System.Drawing.Point(0, 0);
            this.splitContainer15.Name = "splitContainer15";
            this.splitContainer15.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer15.Panel1
            // 
            this.splitContainer15.Panel1.Controls.Add(this.label10);
            // 
            // splitContainer15.Panel2
            // 
            this.splitContainer15.Panel2.Controls.Add(this.dgvLearnHidden);
            this.splitContainer15.Size = new System.Drawing.Size(604, 678);
            this.splitContainer15.SplitterDistance = 25;
            this.splitContainer15.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(220, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(199, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Poids couche cachée - couche de sortie";
            // 
            // dgvLearnHidden
            // 
            this.dgvLearnHidden.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLearnHidden.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLearnHidden.Location = new System.Drawing.Point(0, 0);
            this.dgvLearnHidden.Name = "dgvLearnHidden";
            this.dgvLearnHidden.Size = new System.Drawing.Size(604, 649);
            this.dgvLearnHidden.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.splitContainer16);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1213, 706);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Prédiction un pas";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // splitContainer16
            // 
            this.splitContainer16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer16.Location = new System.Drawing.Point(0, 0);
            this.splitContainer16.Name = "splitContainer16";
            this.splitContainer16.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer16.Panel1
            // 
            this.splitContainer16.Panel1.Controls.Add(this.splitContainer18);
            // 
            // splitContainer16.Panel2
            // 
            this.splitContainer16.Panel2.Controls.Add(this.splitContainer19);
            this.splitContainer16.Size = new System.Drawing.Size(1213, 706);
            this.splitContainer16.SplitterDistance = 34;
            this.splitContainer16.TabIndex = 0;
            // 
            // splitContainer18
            // 
            this.splitContainer18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer18.Location = new System.Drawing.Point(0, 0);
            this.splitContainer18.Name = "splitContainer18";
            // 
            // splitContainer18.Panel1
            // 
            this.splitContainer18.Panel1.Controls.Add(this.txtPredictionOneCount);
            this.splitContainer18.Panel1.Controls.Add(this.label12);
            this.splitContainer18.Panel1.Controls.Add(this.txtPredictionOneN0);
            this.splitContainer18.Panel1.Controls.Add(this.label11);
            // 
            // splitContainer18.Panel2
            // 
            this.splitContainer18.Panel2.Controls.Add(this.btnPredictionOne);
            this.splitContainer18.Size = new System.Drawing.Size(1213, 34);
            this.splitContainer18.SplitterDistance = 900;
            this.splitContainer18.TabIndex = 0;
            // 
            // txtPredictionOneCount
            // 
            this.txtPredictionOneCount.Enabled = false;
            this.txtPredictionOneCount.Location = new System.Drawing.Point(277, 8);
            this.txtPredictionOneCount.Name = "txtPredictionOneCount";
            this.txtPredictionOneCount.Size = new System.Drawing.Size(100, 20);
            this.txtPredictionOneCount.TabIndex = 3;
            this.txtPredictionOneCount.Text = "10";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(183, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Nombre à prédire";
            // 
            // txtPredictionOneN0
            // 
            this.txtPredictionOneN0.Enabled = false;
            this.txtPredictionOneN0.Location = new System.Drawing.Point(33, 7);
            this.txtPredictionOneN0.Name = "txtPredictionOneN0";
            this.txtPredictionOneN0.Size = new System.Drawing.Size(100, 20);
            this.txtPredictionOneN0.TabIndex = 1;
            this.txtPredictionOneN0.Text = "200";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "n0";
            // 
            // btnPredictionOne
            // 
            this.btnPredictionOne.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPredictionOne.Location = new System.Drawing.Point(0, 0);
            this.btnPredictionOne.Name = "btnPredictionOne";
            this.btnPredictionOne.Size = new System.Drawing.Size(309, 34);
            this.btnPredictionOne.TabIndex = 0;
            this.btnPredictionOne.Text = "Lancer prédiction";
            this.btnPredictionOne.UseVisualStyleBackColor = true;
            this.btnPredictionOne.Click += new System.EventHandler(this.btnPredictionOne_Click);
            // 
            // chartPredictionOne
            // 
            chartArea7.Name = "ChartArea1";
            this.chartPredictionOne.ChartAreas.Add(chartArea7);
            this.chartPredictionOne.Dock = System.Windows.Forms.DockStyle.Fill;
            legend7.Name = "Legend1";
            this.chartPredictionOne.Legends.Add(legend7);
            this.chartPredictionOne.Location = new System.Drawing.Point(0, 0);
            this.chartPredictionOne.Name = "chartPredictionOne";
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series11.Legend = "Legend1";
            series11.Name = "Attendues";
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series12.Legend = "Legend1";
            series12.Name = "Predites";
            this.chartPredictionOne.Series.Add(series11);
            this.chartPredictionOne.Series.Add(series12);
            this.chartPredictionOne.Size = new System.Drawing.Size(809, 668);
            this.chartPredictionOne.TabIndex = 0;
            this.chartPredictionOne.Text = "chart1";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.splitContainer17);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1213, 720);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Prédiction multi pas";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // splitContainer17
            // 
            this.splitContainer17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer17.Location = new System.Drawing.Point(0, 0);
            this.splitContainer17.Name = "splitContainer17";
            this.splitContainer17.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer17.Panel1
            // 
            this.splitContainer17.Panel1.Controls.Add(this.splitContainer21);
            // 
            // splitContainer17.Panel2
            // 
            this.splitContainer17.Panel2.Controls.Add(this.splitContainer20);
            this.splitContainer17.Size = new System.Drawing.Size(1213, 720);
            this.splitContainer17.SplitterDistance = 35;
            this.splitContainer17.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer22);
            this.splitContainer1.Size = new System.Drawing.Size(1221, 795);
            this.splitContainer1.SplitterDistance = 732;
            this.splitContainer1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(83, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "RAMAHOLIMIHASO Samuel Johary";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitContainer19
            // 
            this.splitContainer19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer19.Location = new System.Drawing.Point(0, 0);
            this.splitContainer19.Name = "splitContainer19";
            // 
            // splitContainer19.Panel1
            // 
            this.splitContainer19.Panel1.Controls.Add(this.dgvPredictionOne);
            // 
            // splitContainer19.Panel2
            // 
            this.splitContainer19.Panel2.Controls.Add(this.chartPredictionOne);
            this.splitContainer19.Size = new System.Drawing.Size(1213, 668);
            this.splitContainer19.SplitterDistance = 400;
            this.splitContainer19.TabIndex = 1;
            // 
            // dgvPredictionOne
            // 
            this.dgvPredictionOne.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPredictionOne.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPredictionOne.Location = new System.Drawing.Point(0, 0);
            this.dgvPredictionOne.Name = "dgvPredictionOne";
            this.dgvPredictionOne.Size = new System.Drawing.Size(400, 668);
            this.dgvPredictionOne.TabIndex = 0;
            // 
            // splitContainer20
            // 
            this.splitContainer20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer20.Location = new System.Drawing.Point(0, 0);
            this.splitContainer20.Name = "splitContainer20";
            // 
            // splitContainer20.Panel1
            // 
            this.splitContainer20.Panel1.Controls.Add(this.dgvPredictionMulti);
            // 
            // splitContainer20.Panel2
            // 
            this.splitContainer20.Panel2.Controls.Add(this.chartPredictionMulti);
            this.splitContainer20.Size = new System.Drawing.Size(1213, 681);
            this.splitContainer20.SplitterDistance = 400;
            this.splitContainer20.TabIndex = 0;
            // 
            // chartPredictionMulti
            // 
            chartArea8.Name = "ChartArea1";
            this.chartPredictionMulti.ChartAreas.Add(chartArea8);
            this.chartPredictionMulti.Dock = System.Windows.Forms.DockStyle.Fill;
            legend8.Name = "Legend1";
            this.chartPredictionMulti.Legends.Add(legend8);
            this.chartPredictionMulti.Location = new System.Drawing.Point(0, 0);
            this.chartPredictionMulti.Name = "chartPredictionMulti";
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series13.Legend = "Legend1";
            series13.Name = "Attendues";
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series14.Legend = "Legend1";
            series14.Name = "Prédites";
            this.chartPredictionMulti.Series.Add(series13);
            this.chartPredictionMulti.Series.Add(series14);
            this.chartPredictionMulti.Size = new System.Drawing.Size(809, 681);
            this.chartPredictionMulti.TabIndex = 0;
            this.chartPredictionMulti.Text = "chart1";
            // 
            // splitContainer21
            // 
            this.splitContainer21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer21.Location = new System.Drawing.Point(0, 0);
            this.splitContainer21.Name = "splitContainer21";
            // 
            // splitContainer21.Panel1
            // 
            this.splitContainer21.Panel1.Controls.Add(this.cmbPredictionMultiPas);
            this.splitContainer21.Panel1.Controls.Add(this.label13);
            // 
            // splitContainer21.Panel2
            // 
            this.splitContainer21.Panel2.Controls.Add(this.btnPredictionMulti);
            this.splitContainer21.Size = new System.Drawing.Size(1213, 35);
            this.splitContainer21.SplitterDistance = 900;
            this.splitContainer21.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Pas";
            // 
            // cmbPredictionMultiPas
            // 
            this.cmbPredictionMultiPas.FormattingEnabled = true;
            this.cmbPredictionMultiPas.Items.AddRange(new object[] {
            "3",
            "10",
            "20"});
            this.cmbPredictionMultiPas.Location = new System.Drawing.Point(43, 8);
            this.cmbPredictionMultiPas.Name = "cmbPredictionMultiPas";
            this.cmbPredictionMultiPas.Size = new System.Drawing.Size(121, 21);
            this.cmbPredictionMultiPas.TabIndex = 1;
            this.cmbPredictionMultiPas.Text = "3";
            // 
            // dgvPredictionMulti
            // 
            this.dgvPredictionMulti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPredictionMulti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPredictionMulti.Location = new System.Drawing.Point(0, 0);
            this.dgvPredictionMulti.Name = "dgvPredictionMulti";
            this.dgvPredictionMulti.Size = new System.Drawing.Size(400, 681);
            this.dgvPredictionMulti.TabIndex = 0;
            // 
            // btnPredictionMulti
            // 
            this.btnPredictionMulti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPredictionMulti.Location = new System.Drawing.Point(0, 0);
            this.btnPredictionMulti.Name = "btnPredictionMulti";
            this.btnPredictionMulti.Size = new System.Drawing.Size(309, 35);
            this.btnPredictionMulti.TabIndex = 0;
            this.btnPredictionMulti.Text = "Lancer prédiction multi-pas";
            this.btnPredictionMulti.UseVisualStyleBackColor = true;
            this.btnPredictionMulti.Click += new System.EventHandler(this.btnPredictionMulti_Click);
            // 
            // splitContainer22
            // 
            this.splitContainer22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer22.Location = new System.Drawing.Point(0, 0);
            this.splitContainer22.Name = "splitContainer22";
            // 
            // splitContainer22.Panel1
            // 
            this.splitContainer22.Panel1.Controls.Add(this.splitContainer24);
            // 
            // splitContainer22.Panel2
            // 
            this.splitContainer22.Panel2.Controls.Add(this.splitContainer23);
            this.splitContainer22.Size = new System.Drawing.Size(1221, 59);
            this.splitContainer22.SplitterDistance = 407;
            this.splitContainer22.TabIndex = 0;
            // 
            // splitContainer23
            // 
            this.splitContainer23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer23.Location = new System.Drawing.Point(0, 0);
            this.splitContainer23.Name = "splitContainer23";
            // 
            // splitContainer23.Panel1
            // 
            this.splitContainer23.Panel1.Controls.Add(this.label15);
            // 
            // splitContainer23.Panel2
            // 
            this.splitContainer23.Panel2.Controls.Add(this.label16);
            this.splitContainer23.Size = new System.Drawing.Size(810, 59);
            this.splitContainer23.SplitterDistance = 428;
            this.splitContainer23.TabIndex = 0;
            // 
            // splitContainer24
            // 
            this.splitContainer24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer24.Location = new System.Drawing.Point(0, 0);
            this.splitContainer24.Name = "splitContainer24";
            this.splitContainer24.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer24.Panel1
            // 
            this.splitContainer24.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer24.Panel2
            // 
            this.splitContainer24.Panel2.Controls.Add(this.label14);
            this.splitContainer24.Size = new System.Drawing.Size(407, 59);
            this.splitContainer24.SplitterDistance = 29;
            this.splitContainer24.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(172, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 18);
            this.label14.TabIndex = 1;
            this.label14.Text = "ESIIA 4 n°16";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(89, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(294, 18);
            this.label15.TabIndex = 2;
            this.label15.Text = "Mini-projet : Réseaux de neurones artificiels";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(131, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(117, 18);
            this.label16.TabIndex = 3;
            this.label16.Text = "ISPM 2016-2017";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 795);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "RNA";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHenon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartHenon)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer8.Panel1.ResumeLayout(false);
            this.splitContainer8.Panel1.PerformLayout();
            this.splitContainer8.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).EndInit();
            this.splitContainer8.ResumeLayout(false);
            this.splitContainer9.Panel1.ResumeLayout(false);
            this.splitContainer9.Panel2.ResumeLayout(false);
            this.splitContainer9.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).EndInit();
            this.splitContainer9.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            this.splitContainer10.Panel1.ResumeLayout(false);
            this.splitContainer10.Panel1.PerformLayout();
            this.splitContainer10.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).EndInit();
            this.splitContainer10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvXBar)).EndInit();
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            this.splitContainer11.Panel1.ResumeLayout(false);
            this.splitContainer11.Panel1.PerformLayout();
            this.splitContainer11.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).EndInit();
            this.splitContainer11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEigenValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTakens)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.splitContainer12.Panel1.ResumeLayout(false);
            this.splitContainer12.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).EndInit();
            this.splitContainer12.ResumeLayout(false);
            this.splitContainer13.Panel1.ResumeLayout(false);
            this.splitContainer13.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).EndInit();
            this.splitContainer13.ResumeLayout(false);
            this.splitContainer14.Panel1.ResumeLayout(false);
            this.splitContainer14.Panel1.PerformLayout();
            this.splitContainer14.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).EndInit();
            this.splitContainer14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLearnInput)).EndInit();
            this.splitContainer15.Panel1.ResumeLayout(false);
            this.splitContainer15.Panel1.PerformLayout();
            this.splitContainer15.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).EndInit();
            this.splitContainer15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLearnHidden)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.splitContainer16.Panel1.ResumeLayout(false);
            this.splitContainer16.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).EndInit();
            this.splitContainer16.ResumeLayout(false);
            this.splitContainer18.Panel1.ResumeLayout(false);
            this.splitContainer18.Panel1.PerformLayout();
            this.splitContainer18.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer18)).EndInit();
            this.splitContainer18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPredictionOne)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.splitContainer17.Panel1.ResumeLayout(false);
            this.splitContainer17.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer17)).EndInit();
            this.splitContainer17.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer19.Panel1.ResumeLayout(false);
            this.splitContainer19.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer19)).EndInit();
            this.splitContainer19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPredictionOne)).EndInit();
            this.splitContainer20.Panel1.ResumeLayout(false);
            this.splitContainer20.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer20)).EndInit();
            this.splitContainer20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPredictionMulti)).EndInit();
            this.splitContainer21.Panel1.ResumeLayout(false);
            this.splitContainer21.Panel1.PerformLayout();
            this.splitContainer21.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer21)).EndInit();
            this.splitContainer21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPredictionMulti)).EndInit();
            this.splitContainer22.Panel1.ResumeLayout(false);
            this.splitContainer22.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer22)).EndInit();
            this.splitContainer22.ResumeLayout(false);
            this.splitContainer23.Panel1.ResumeLayout(false);
            this.splitContainer23.Panel1.PerformLayout();
            this.splitContainer23.Panel2.ResumeLayout(false);
            this.splitContainer23.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer23)).EndInit();
            this.splitContainer23.ResumeLayout(false);
            this.splitContainer24.Panel1.ResumeLayout(false);
            this.splitContainer24.Panel1.PerformLayout();
            this.splitContainer24.Panel2.ResumeLayout(false);
            this.splitContainer24.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer24)).EndInit();
            this.splitContainer24.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.TextBox txtN;
        private System.Windows.Forms.Label lblN;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnHenonCompute;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridView dgvHenon;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartHenon;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.DataGridView dgvXBar;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.DataGridView dgvEigenValues;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTakens;
        private System.Windows.Forms.SplitContainer splitContainer8;
        private System.Windows.Forms.TextBox txtTakensTau;
        private System.Windows.Forms.Label lblTakensTau;
        private System.Windows.Forms.TextBox txtTakensN;
        private System.Windows.Forms.Label lblTakensN;
        private System.Windows.Forms.TextBox txtTakensI;
        private System.Windows.Forms.Label lblTakensI;
        private System.Windows.Forms.SplitContainer splitContainer9;
        private System.Windows.Forms.Button btnArchitectureCompute;
        private System.Windows.Forms.Label lblTakensCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.SplitContainer splitContainer10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.SplitContainer splitContainer11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblTakensHidden;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.SplitContainer splitContainer12;
        private System.Windows.Forms.Button btnLearn;
        private System.Windows.Forms.SplitContainer splitContainer13;
        private System.Windows.Forms.SplitContainer splitContainer14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dgvLearnInput;
        private System.Windows.Forms.SplitContainer splitContainer15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvLearnHidden;
        private System.Windows.Forms.SplitContainer splitContainer16;
        private System.Windows.Forms.Button btnPredictionOne;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPredictionOne;
        private System.Windows.Forms.SplitContainer splitContainer18;
        private System.Windows.Forms.TextBox txtPredictionOneCount;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPredictionOneN0;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.SplitContainer splitContainer17;
        private System.Windows.Forms.SplitContainer splitContainer19;
        private System.Windows.Forms.DataGridView dgvPredictionOne;
        private System.Windows.Forms.SplitContainer splitContainer21;
        private System.Windows.Forms.ComboBox cmbPredictionMultiPas;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnPredictionMulti;
        private System.Windows.Forms.SplitContainer splitContainer20;
        private System.Windows.Forms.DataGridView dgvPredictionMulti;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPredictionMulti;
        private System.Windows.Forms.SplitContainer splitContainer22;
        private System.Windows.Forms.SplitContainer splitContainer24;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.SplitContainer splitContainer23;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
    }
}

