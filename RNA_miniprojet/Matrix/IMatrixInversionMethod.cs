﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.Matrix
{
    interface IMatrixInversionMethod
    {
        Matrix Inverse(Matrix m);
    }
}
