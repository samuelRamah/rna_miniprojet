﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.Matrix
{
    class StandardInversion : IMatrixInversionMethod
    {
        public Matrix Inverse(Matrix m)
        {
            Matrix result = m;

            // Create increased matrix
            Matrix iMat = new Matrix(new double[m.Lines, m.Columns * 2]);
            for (int i = 0; i < iMat.Lines; i++)
            {
                for (int j = 0; j < iMat.Columns; j++)
                {
                    if (j < m.Columns) iMat[i, j] = m[i, j];
                    else
                    {
                        if ((j - m.Columns) == i) iMat[i, j] = 1;
                        else iMat[i, j] = 0;
                    }
                }
            }

            // Apply Gauss pivot method
            for (int i = 0; i < iMat.Lines - 1; i++)
            {
                //int pivot = searchPivot(iMat, i, i);
                //if (pivot == -1) throw new MatrixException("Cannot compute Inverse matrix");

                //iMat = iMat.ExchangeColumns(i, pivot);

                for (int j = i + 1; j < iMat.Lines; j++)
                {
                    double factor = iMat[j, i] / iMat[i, i];

                    for (int k = i; k < iMat.Columns; k++)
                    {
                        iMat[j, k] -= factor * iMat[i, k];
                    }
                }
            }

            // Compute inverse
            result = new Matrix(m.Lines, m.Columns);
            for (int i = m.Lines - 1; i >= 0; i--)
            {
                for (int j = 0; j < m.Columns; j++)
                {
                    if (i == (m.Lines - 1))
                    {
                        result[i, j] = iMat[i, (j + m.Columns)] / iMat[i, (m.Columns - 1)];
                    }
                    else
                    {
                        result[i, j] = iMat[i, j + m.Columns];
                        for (int k = m.Columns - 1; k >= (i + 1); k--)
                        {
                            result[i, j] -= iMat[i, k] * result[k, j];
                        }
                        result[i, j] /= iMat[i, i];
                    }
                } 
            }

            return result;
        }

        private int searchPivot(Matrix m, int fromLine, int fromColumn)
        {
            int pivot = m[fromLine, fromColumn] == 0 ? -1 : fromColumn;

            double max = Math.Abs(m[fromLine, fromColumn]);

            for (int i = fromColumn + 1; i < m.Columns; i++)
            {
                if (Math.Abs(m[fromLine, i]) > max)
                {
                    max = Math.Abs(m[fromLine, i]);
                    pivot = i;
                }
            }

            return pivot;
        }
    }
}
