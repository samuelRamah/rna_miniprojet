﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.Matrix
{
    class MatrixException: Exception
    {
        public MatrixException(): base()
        {
           
        }

        public MatrixException(string message): base(message)
        {

        }
    }
}
