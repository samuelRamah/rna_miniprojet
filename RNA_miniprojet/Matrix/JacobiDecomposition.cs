﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.Matrix
{
    class JacobiDecomposition : IMatrixDecompositionMethod
    {
        public double[] GetEigenValues(Matrix m)
        {
            HashSet<double> values = new HashSet<double>();
            Matrix D = generateDiagonal(m);
            for (int i = 0; i < D.Lines; i++)
            {
                values.Add(D[i, i]);
            }

            return values.ToArray<double>();
        }

        private void chooseJKIndexes(Matrix A, ref int j, ref int k)
        {
            // Choose j and k indexes
            j = 0;
            k = 0;

            double max = double.MinValue;
            for (int i = 0; i < A.Lines; i++)
            {
                for (int n = i + 1; n < A.Columns; n++)
                {
                    double toCompare = Math.Abs(A[i, n]);
                    if (n != i && toCompare > max)
                    {
                        max = toCompare;
                        j = i;
                        k = n;
                    }
                }
            }
        }

        private Matrix createMatrixP(Matrix A, int j, int k)
        {
            Matrix P = new Matrix(A.Lines, A.Columns);
            P.SetDiagonalToOne();

            // Compute (Pi)jj, (Pi)kk, (Pi)jk and (Pi)kj
            double Pijj, Pikk, Pikj, Pijk;
            if (A[j, j] != A[k, k])
            {
                // Init b and c factors
                double b = Math.Abs(A[j, j] - A[k, k]);
                double c = 2.0 * A[j, k] * Math.Sign(A[j, j] - A[k, k]);

                // Compute
                Pijj = Pikk = Math.Sqrt(0.5 * (1.0 + (b / Math.Sqrt(c * c + b * b))));
                Pikj = c / (2.0 * Pijj * Math.Sqrt(c * c + b * b));
                Pijk = -Pikj;
            }
            else
            {
                Pijj = Pikk = Math.Sqrt(2) / 2.0;
                Pikj = Pijj;
                Pijk = -Pikj;
            }

            // Assign values to the matrix P
            P[j, k] = Pijk;
            P[k, j] = Pikj;
            P[j, j] = Pijj;
            P[k, k] = Pikk;

            return P;
        }

        private Matrix generateDiagonal(Matrix A, int i = 0)
        {
            if (stopTest(A, i))
            {
                return A;
            }

            int j = 0, k = 0;
            chooseJKIndexes(A, ref j, ref k);

            Matrix P = createMatrixP(A, j, k);

            // Create matrix P-1 (optimized)
            Matrix P1 = new Matrix((double[,])P.Data.Clone());
            P1[j, k] = P[k, j];
            P1[k, j] = P[j, k];

            // D = P1 * A
            Matrix tmp = new Matrix((double[,])A.Data.Clone());
            for (int idx = 0; idx < tmp.Columns; idx++)
            {
                tmp[j, idx] = A[j, idx] * P1[j, j] + A[k, idx] * P1[j, k];
                tmp[k, idx] = A[j, idx] * P1[k, j] + A[k, idx] * P1[k, k];
            }

            Matrix D = new Matrix((double[,])tmp.Data.Clone());
            // D = D * P
            for (int idx = 0; idx < D.Lines; idx++)
            {
                D[idx, j] = tmp[idx, j] * P[j, j] + tmp[idx, k] * P[k, j];
                D[idx, k] = tmp[idx, j] * P[j, k] + tmp[idx, k] * P[k, k];
            }

            return generateDiagonal(D, i + 1);
        }

        private bool stopTest(Matrix A, int i)
        {
            if (i > 60) return true;

            // Compute Error factor
            double E = 0;
            for (int j = 0; j < A.Lines; j++)
            {
                for (int k = 0; k < A.Columns; k++)
                {
                    if (j != k)
                    {
                        E += Math.Abs(A[j, k]);
                    }
                }
            }
            double epsilon = 1e-10;
            E = Math.Abs(E);
            if (E < epsilon) return true;

            return false;
        }
    }
}
