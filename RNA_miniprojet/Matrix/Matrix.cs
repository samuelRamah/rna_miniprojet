﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNA_miniprojet.Matrix
{
    class Matrix
    {
        private double[,] data;
        public double[,] Data
        {
            get
            {
                return this.data;
            }
        }

        public double this[int i, int j]
        {
            get
            {
                return getData(i, j);
            }   
            set
            {
                setData(i, j, value);
            }
        }

        private void checkIndexes(int i, int j)
        {
            if (data == null)
            {
                throw new MatrixException("Data array not initialized yet.");
            }

            if (i < 0 || i > data.GetLength(0))
            {
                throw new MatrixException(
                    $"The index of the line is out of bound. It should be positive and the maximum is { data.GetLength(0) } but you set { i }.");
            }
            if (j < 0 || j > data.GetLength(1))
            {
                throw new MatrixException(
                    $"The index of the column is out of bound. It should be positive and the maximum is { data.GetLength(1) } but you set { j }.");
            }
        }
        private double getData(int i, int j)
        {
            checkIndexes(i, j);
            return data[i, j];
        }
        private void setData(int i, int j, double value)
        {
            checkIndexes(i, j);
            data[i, j] = value;
        }
        private void setDataArray(double[,] value)
        {
            if (value == null || value.GetLength(0) <= 0 || value.GetLength(1) <= 0)
            {
                throw new MatrixException("Invalid array for the data initialisation.");
            }
            data = value;
        }

        public Matrix()
        {
            decomposer = new JacobiDecomposition();
            inverter = new StandardInversion();
        }

        public Matrix(double[,] data): this()
        {
            setDataArray(data);
        }

        public Matrix(int lines, int columns): this()
        {
            if (lines <= 0 || columns <= 0)
            {
                throw new MatrixException("Invalid line or columns size in initialisation.");
            }
            data = new double[lines, columns];
        }

        public int Lines
        {
            get
            {
                return data.GetLength(0);
            }
        }
        public int Columns
        {
            get
            {
                return data.GetLength(1);
            }
        }
        
        private void checkForAdditionSubtraction(Matrix a, Matrix b)
        {
            if (a.Lines != b.Lines)
            {
                throw new MatrixException("Cannot perform addition or subtraction on two matrices with different lines count.");
            }
            if (a.Columns != b.Columns)
            {
                throw new MatrixException("Cannot perform addition or subtraction on two matrices with different columns count.");
            }
        }

        public Matrix Add(Matrix m)
        {
            checkForAdditionSubtraction(this, m);
            Matrix result = new Matrix(this.Lines, this.Columns);
            for (int i = 0; i < Lines; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = this[i, j] + m[i, j];
                }
            }
            return result;
        }

        public Matrix Subtract(Matrix m)
        {
            checkForAdditionSubtraction(this, m);
            Matrix result = new Matrix(Lines, Columns);
            for (int i = 0; i < Lines; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = this[i, j] - m[i, j];
                }
            }
            return result;
        }

        private void checkForMultiplication(Matrix a, Matrix b)
        {
            if (a.Columns != b.Lines)
            {
                throw new MatrixException(
                    "Cannot perform multiplication because the number of columns of the left matrix is different of the number of lines of the right matrix.");
            }
        }

        public Matrix Multiply(Matrix m)
        {
            checkForMultiplication(this, m);
            Matrix result = new Matrix(this.Lines, m.Columns);

            for (int i = 0; i < this.Lines; i++)
            {
                for (int j = 0; j < m.Columns; j++)
                {
                    result[i, j] = 0;
                    for (int k = 0; k < m.Lines; k++)
                    {
                        result[i, j] += this[i, k] * m[k, j];
                    }
                }
            }

            return result;
        }

        public Matrix Multiply(double a)
        {
            Matrix result = new Matrix(Lines, Columns);
            for (int i = 0; i < Lines; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = a * this[i, j];
                }
            }
            return result;
        }

        public static Matrix operator +(Matrix a, Matrix b)
        {
            return a.Add(b);
        }

        public static Matrix operator -(Matrix a, Matrix b)
        {
            return a.Subtract(b);
        }

        public static Matrix operator *(Matrix a, Matrix b)
        {
            return a.Multiply(b);
        }

        public static Matrix operator *(Matrix a, double b)
        {
            return a.Multiply(b);
        }

        public Matrix Transpose()
        {
            Matrix result = new Matrix(Columns, Lines);

            for (int i = 0; i < Lines; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[j, i] = this[i, j];
                }
            }

            return result;
        }

        // Operator definition which transpose a matrix
        public static Matrix operator ~(Matrix a)
        {
            return a.Transpose();
        }

        private IMatrixDecompositionMethod decomposer;
        private IMatrixInversionMethod inverter;

        public Matrix Inverse()
        {
            return inverter.Inverse(this);
        }

        // Operator definition which invert a matrix
        public static Matrix operator !(Matrix m)
        {
            return m.Inverse();
        }

        public double[] EigenValues()
        {
            return decomposer.GetEigenValues(this);
        }

        private void setDiagonalTo(double value)
        {
            Array.Clear(data, 0, data.Length);
            for (int i = 0; i < data.GetLength(0); i++)
            {
                if (data.GetLength(1) < i) break;
                data[i, i] = value;
            }
        }

        public void SetDiagonalToOne() { setDiagonalTo(1); }
        private void setDiagonalToZero() { Array.Clear(data, 0, data.Length); }

        public override string ToString()
        {
            if (data == null) return "";
            String s = "";

            for (int i = 0; i < Lines; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    s += $"{data[i, j]}   ";
                }
                s += "\n";
            }

            return s;
        }

        public Matrix ExchangeLines(int x, int y)
        {
            checkIndexes(x, y);

            if (x == y) return this;

            double[,] newData = (double[,])data.Clone();

            for (int i = 0; i < Columns; i++)
            {
                newData[x, i] = data[y, i];
                newData[y, i] = data[x, i];
            }

            return new Matrix(newData);
        }

        public Matrix ExchangeColumns(int x, int y)
        {
            checkIndexes(x, y);

            if (x == y) return this;

            double[,] newData = (double[,])data.Clone();

            for (int i = 0; i < Lines; i++)
            {
                newData[i, x] = data[i, y];
                newData[i, y] = data[i, x];
            }

            return new Matrix(newData);
        }
    }
}
